<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use App\Models\StudentsCourse;
use App\Models\Teacher;
use App\Models\TeachersCourse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'password' => 'required',
            'email' => 'required'
        ]);
        $user = new User();
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        return response()->json(['message' => 'You have been registerd succesfuly']);
    }
    public function login(Request $request)
    {
        if(Auth::attempt($request->all())){
            $user = Auth::user();
            $user->tokens()->delete();
            $token = auth()->user()->createToken('API Token')->plainTextToken;
            return response()->json(['message' => 'You are logged in','user' => $user , 'token' => $token]);
        }
        else{
            return response()->json(['message' => 'Unauthorized'],403);
        }
    }
    public function get_students()
    {
        $students = Student::get();
        foreach($students as $student){
            $student->courses = $student->courses;
        }
        foreach($student->courses as $course){
            $course->course_name = $course->course->name;
            $course->teacher_name = $course->teacher->name;
        }
        return response()->json($students);
    }
    public function register_student(Request $request)
    {
        $student = new Student();
        $student->name = $request->student['name'];
        $student->save();

        foreach($request->courses as $c){
            $course = new Course();
            $course->name = $c['name'];
            $course->save();

                // create new teacher
                $teacher = new Teacher();
                $teacher->name = $c['teacher']['name'];
                $teacher->save();
                // assign course to teacher
                $teacher_course = new TeachersCourse();
                $teacher_course->teacher_id = $teacher->id;
                $teacher_course->course_id = $course->id;
                $teacher_course->save();
                // assign student to teacher course
                $student_course = new StudentsCourse();
                $student_course->student_id = $student->id;
                $student_course->teachers_course_id = $teacher_course->id;
                $student_course->save();
        }

        return response()->json(['message' => 'student registered succesfuly','student' => $student]);
    }
    public function update_student(Request $request ,$student_id)
    {
        $student = Student::where('id',$student_id)->first();

        StudentsCourse::where('student_id',$student->id)->delete();
        foreach($request->courses as $c){
            $course = new Course();
            $course->name = $c['name'];
            $course->save();

                // create new teacher
                $teacher = new Teacher();
                $teacher->name = $c['teacher']['name'];
                $teacher->save();
                // assign course to teacher
                $teacher_course = new TeachersCourse();
                $teacher_course->teacher_id = $teacher->id;
                $teacher_course->course_id = $course->id;
                $teacher_course->save();
                // assign student to teacher course
                $student_course = new StudentsCourse();
                $student_course->student_id = $student->id;
                $student_course->teachers_course_id = $teacher_course->id;
                $student_course->save();
        }
        return response()->json(['message' => 'student updated succesfuly','student' => $student]);

    }
}
